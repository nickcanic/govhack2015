﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using GovHack2015.Controllers;
using GovHack2015.Database;

namespace GovHack2015
{
    public class AgoDataDependencyResolver : IDependencyResolver
    {
        public void Dispose()
        {
            // must be no-op
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == typeof (AgorController))
            {

                var data = Extract.CreateSnapshots();
                var aaoData = Extract.LoadAaoEntries();

                var cacheOfPortfolioNames = new HashSet<string>(data.SelectMany(d => d.AgorEntries.Select(e => e.Portfolio))); // not working for some reason
                
                return new AgorController(data, cacheOfPortfolioNames, aaoData);
            }

            return null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return new List<object>();
        }

        public IDependencyScope BeginScope()
        {
            return this;
        }
    }
}