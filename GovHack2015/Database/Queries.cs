﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using GovHack2015.Controllers;
using GovHack2015.Models.AAO;
using GovHack2015.Models.AGOR;

namespace GovHack2015.Database
{
    public static class Queries
    {
        public static IEnumerable<string> GetActsForPortfolioAtDate(DateTime date, IEnumerable<AaoEntry> entries,
            string portfolioName)
        {

            var relevantEntries = entries
                .Where(e => e.Portfolio.ToLower() == portfolioName.ToLower())
                .Where(e => e.Date <= date)
                .ToList();

            var mostRecentEntries = relevantEntries.GroupBy(g => g.Date).OrderByDescending(g => g.Key).FirstOrDefault();

            if (mostRecentEntries == null)
                return new List<string>();
            else
            {
                return mostRecentEntries.Select(e => e.Act);
            }
        }
 
        
        public static SinglePortfolioDto GetPortfolioAggregates(IEnumerable<AgorSnapshot> snapshots, string portfolioName)
        {
            var rowsByDate = (from dataSet in snapshots
                select new
                {
                    Date = dataSet.Date,
                    Data = dataSet.AgorEntries.Where(e => e.Portfolio == portfolioName)
                }).ToList();

            var sb = new StringBuilder();
            var headerRow = "Date,ASL,Appropriations ($M),Expenses ($M)";
            sb.AppendLine(headerRow);
            
            foreach (var dataSet in rowsByDate)
            {
                var date = dataSet.Date.Date.ToShortDateString();
                var aggregateAsl = dataSet.Data.Aggregate(0, (i, entry) => (int) (i + (entry.Asl ?? 0)));
                var aggregateAppropriations = dataSet.Data.Aggregate(0D, (i, entry) => i + (entry.Appropriations ?? 0D));
                var aggregateExpenses = dataSet.Data.Aggregate(0D, (d, entry) => d + (entry.Expenses ?? 0D));
                sb.AppendLine(String.Format("{0},{1},{2},{3}", date, aggregateAsl, aggregateAppropriations,
                    aggregateExpenses));
            }

            return new SinglePortfolioDto()
            {
                PortfolioName = portfolioName,
                DataCsv = sb.ToString()
            };
        }




        
    }
}