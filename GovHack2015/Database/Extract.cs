﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects.DataClasses;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using GovHack2015.Models.AAO;
using GovHack2015.Models.AGOR;
using Microsoft.Owin.Security.DataHandler;
using Microsoft.VisualBasic.FileIO;

namespace GovHack2015.Database
{
    public static class Extract
    {

        public static IEnumerable<AaoEntry> LoadAaoEntries()
        {
            IList<AaoEntry> entries = new List<AaoEntry>();
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(Properties.Resources.govhackAAOdata));

            using (TextFieldParser parser = new TextFieldParser(memStream))
            {
                parser.Delimiters = new string[] { "," };
                parser.ReadFields(); // gets the column headings, advances reader one line
                while (true)
                {
                    var row = parser.ReadFields();
                    if (row == null)
                    {
                        break;
                    }

                    var entry = new AaoEntry();
                    entry.Date = DateTime.ParseExact(row[0].Trim(),"d",CultureInfo.CreateSpecificCulture("en-AU"));
                    entry.Portfolio = row[1].Trim();
                    entry.Act = row[2].Trim();
                    entries.Add(entry);
                }
            }

            return entries;
        }
        

        public static IEnumerable<AgorEntry> ExtractEntries(string dataCsv)
        {
            IList<AgorEntry> entries = new List<AgorEntry>();
            
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(dataCsv));
            
            using (TextFieldParser parser = new TextFieldParser(memStream))
            {
                parser.Delimiters = new string[] { "," };
                parser.ReadFields(); // gets the column headings, advances reader one line
                while (true)
                {
                    var row = parser.ReadFields();
                    if (row == null)
                    {
                        break;
                    }
                    
                    var entry = new AgorEntry();
                    entry.Entity = row[0].Trim();
                    entry.Abn = row[18].Trim();
                    entry.Portfolio = row[1].Trim();
                    entry.Asl =  String.IsNullOrWhiteSpace(row[12]) ? (int?) null : Convert.ToInt16(Regex.Replace(row[12],@",",""));
                    entry.PrimaryIfSecondary = row[19].Trim();
                    entry.Appropriations = String.IsNullOrWhiteSpace(row[20]) ? (double?)null : Convert.ToDouble(row[20].Trim());
                    entry.Expenses = String.IsNullOrWhiteSpace(row[21]) ? (double?)null :  Convert.ToDouble(row[21].Trim());
                    
                    entries.Add(entry);
                }
            }
            
            return entries;
        }

        public static IEnumerable<AgorSnapshot> CreateSnapshots()
        {
            // Canberra is GMT +10
            var s1 = new AgorSnapshot()
                     {
                         AgorEntries = ExtractEntries(Properties.Resources._2014_12_11).ToList(),
                         Date = new DateTimeOffset(new DateTime(2014,12,11),TimeSpan.FromHours(10))
                     };

            var s2 = new AgorSnapshot()
                     {
                         AgorEntries = ExtractEntries(Properties.Resources._2015_2_16).ToList(),
                         Date = new DateTimeOffset(new DateTime(2015, 2, 16),TimeSpan.FromHours(10))
                     };

            var s3 = new AgorSnapshot()
                     {
                         AgorEntries = ExtractEntries(Properties.Resources._2015_04_24).ToList(),
                         Date = new DateTimeOffset(new DateTime(2015, 4, 24), TimeSpan.FromHours(10))
                     };

            var s4 = new AgorSnapshot()
                     {
                         AgorEntries = ExtractEntries(Properties.Resources._2015_05_18).ToList(),
                         Date = new DateTimeOffset(new DateTime(2015, 5, 18), TimeSpan.FromHours(10))
                     };


            return new List<AgorSnapshot>() {s1, s2, s3, s4};
        }

        
   
    }
}