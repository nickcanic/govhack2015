﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GovHack2015.Database
{
    public class AgorDbInitialiser :  System.Data.Entity.DropCreateDatabaseIfModelChanges<AgorContext>
    {
        protected override void Seed(AgorContext context)
        {
            var data = Extract.CreateSnapshots();
            foreach (var datum in data)
            {
                context.AgorSnapshots.Add(datum);
            }

            context.SaveChanges();
        }
    }
}