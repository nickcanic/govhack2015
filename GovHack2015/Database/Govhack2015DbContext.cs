﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using GovHack2015.Models.AGOR;

namespace GovHack2015.Database
{
    public class AgorContext : DbContext
    {
        public AgorContext() : base("AgorContext")
        {
           
        }

        public DbSet<AgorSnapshot> AgorSnapshots { get; set; }
        public DbSet<AgorEntry> AgorEntries { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }


    
}