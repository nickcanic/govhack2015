﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Razor.Generator;
using GovHack2015.Database;
using GovHack2015.Models.AAO;
using GovHack2015.Models.AGOR;

namespace GovHack2015.Controllers
{
    public class AgorController : ApiController
    {
        private IEnumerable<AgorSnapshot> _data;
        private readonly HashSet<string> _cacheOfPortfolioNames;
        private readonly IEnumerable<AaoEntry> _aaoEntries;

        // DI not working for some reason
        public AgorController(IEnumerable<AgorSnapshot> data, HashSet<string> cacheOfPortfolioNames, IEnumerable<AaoEntry> aaoEntries)
        {
            _data = data;
            _cacheOfPortfolioNames = cacheOfPortfolioNames;
            _aaoEntries = aaoEntries;
        }
        
        public AgorController()
        {
            _data = Extract.CreateSnapshots();
            _cacheOfPortfolioNames = new HashSet<string>(_data.SelectMany(d => d.AgorEntries.Select(e => e.Portfolio)));
            _aaoEntries = Extract.LoadAaoEntries();
        }

        [HttpGet]
        [Route("AdministeredActs/{portfolio}/{date}")]
        public IHttpActionResult GetAdministeredActs([FromUri] string portfolio, [FromUri] string date)
        {
            if (!Regex.IsMatch(date, @"[0-9]{4,4}-[0-9]{1,2}-[0-9]{1,2}"))
                return BadRequest("Date must be in format yyyy-mm-dd");
            
            var parsedDate = Convert.ToDateTime(date);

            var result = Queries.GetActsForPortfolioAtDate(parsedDate, _aaoEntries, portfolio).ToList();
            return Ok(result);
        }

        
        [HttpGet]
        [Route("PortfolioAggregates/{portfolioName}")]
        public IHttpActionResult GetPortfolioAggregates([FromUri] string portfolioName)
        {

            // dependency injected cache was null for some reason, who knows why ???
           if (!_cacheOfPortfolioNames.Contains(portfolioName))
                return BadRequest(String.Format("Portfolio name '{0}' does not exist in the AGOR data.", portfolioName));
            var result = Queries.GetPortfolioAggregates(_data, portfolioName);
            
            return Ok(result.DataCsv);
        }


        [HttpGet]
        [Route("SingleAgoByName/{entityName}")]
        public IHttpActionResult GetSingleAgoByName([FromUri] string entityName)
        {
            var rowsByDate = (from dataSet in _data
                select new
                       {
                           Date = dataSet.Date,
                           Data = dataSet.AgorEntries.Where(e => e.Entity == entityName).SingleOrDefault()
                       }).ToList();

            if (rowsByDate.All(r => r.Data == null))
            {
                return
                    BadRequest(String.Format("Cannot find AGOR entity name: '{0}'.  Ensure you have typed the name exactly as it appears in the AGOR data.",entityName));
            }


            var sb = new StringBuilder();
            var headerRow = "Date,ASL,Appropriations ($M),Expenses ($M)";
            sb.AppendLine(headerRow);
            foreach (var dataSet in rowsByDate)
            {
                var toAppend = String.Format("{0},{1},{2},{3},",
                    dataSet.Date.Date.ToShortDateString(),
                   // dataSet.Data.Portfolio,  // data is too boring to show
               //     dataSet.Data.Abn,  // data is too boring to show
                    dataSet.Data.Asl,
                    dataSet.Data.Appropriations,
                    dataSet.Data.Expenses);

                sb.AppendLine(toAppend);
            }
            
            return Ok(sb.ToString());
        }
        
    }



    public class SinglePortfolioDto
    {
        public string PortfolioName { get; set; }
        public string DataCsv { get; set; }
    }
    
}
