﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GovHack2015.Startup))]
namespace GovHack2015
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
