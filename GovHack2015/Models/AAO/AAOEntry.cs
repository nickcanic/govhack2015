﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GovHack2015.Models.AAO
{
    public class AaoEntry
    {
        public DateTimeOffset Date { get; set; }
        public string Portfolio { get; set; }
        public string Act { get; set; }

        public override string ToString()
        {
            return string.Format("Portfolio: {0}, Date: {1}, Act: {2}", Portfolio, Date, Act);
        }
    }
}