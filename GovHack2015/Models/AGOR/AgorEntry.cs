﻿using System;
using System.Collections;

namespace GovHack2015.Models.AGOR
{
    public class AgorEntry
    {
        public int ID { get; set; }
        public string Entity { get; set; }
        public string Portfolio { get; set; }
        public string Abn { get; set; }
        public int? Asl { get; set; }
      
        public double? Appropriations { get; set; }
        public double? Expenses { get; set; }
        public string PrimaryIfSecondary { get; set; }
        public override string ToString()
        {
            return string.Format("ID: {0}, Entity: {1}", ID, Entity);
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}