﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GovHack2015.Models.AGOR
{
    public class AgorSnapshot
    {
        public int ID { get; set; }
        public DateTimeOffset Date { get; set; }
        public virtual ICollection<AgorEntry> AgorEntries { get; set; } 
    }
}