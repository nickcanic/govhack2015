﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace GovHack2015.Tests
{
    [TestFixture]
    public class ExtractTests
    {

        [Test]
        public void ParseToEntriesFirstSheet()
        {
            var testData = Properties.Resources._2014_12_11;
            var result = GovHack2015.Database.Extract.ExtractEntries(testData);
            Assert.IsTrue(result.Count() == 1192);
        }

        [Test]
        public void ParseToEntriesSecondSheet()
        {
            var testData = Properties.Resources._2015_2_16;
            var result = GovHack2015.Database.Extract.ExtractEntries(testData);
            Assert.IsTrue(result.Count() == 1187);
        }

        [Test]
        public void ParseToEntriesThirdSheet()
        {
            var testData = Properties.Resources._2015_04_24;
            var result = GovHack2015.Database.Extract.ExtractEntries(testData);
            Assert.IsTrue(result.Count() == 1182);
        }

        [Test]
        public void ParseToEntriesFourthSheet()
        {
            var testData = Properties.Resources._2015_05_18;
            var result = GovHack2015.Database.Extract.ExtractEntries(testData);
            Assert.IsTrue(result.Count() == 1182); // same as last
        }

        [Test]
        public void CreateSnapshotObjects()
        {
            var result = GovHack2015.Database.Extract.CreateSnapshots();
            Assert.IsTrue(result.Count() == 4);

        }

        [Test]
        public void GetAaoData()
        {
            var result = GovHack2015.Database.Extract.LoadAaoEntries();
            Assert.IsTrue(result.Any());
        }
     
    }
}
